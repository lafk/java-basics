# The War Card Game

The task is set: implement full War Card Game.
UI for console is good enough.

1. Players receive more or less equal amount of cards, by splitting the deck between themselves.
2. Deck consists of 52 cards, no jokers.
3. Player simultaneously reveal their top card.
4. Cards are compared by value only, higher one wins.
5. Winner takes both card into his deck (at the bottom).
6. In case of a draw, cards are left as they are and:
6.1. both players take out next card from their decks, without revealing it
6.2. they place unrevealed card on top of tied card
6.3. then they reveal next top card from the deck
6.4. these revealed cards are then compared (as usual, that is value only)
6.5. in case of a draw, steps repeat from 6.1
6.6. in case of somebody winning, he takes ALL the cards (revealed or not) and shuffles them in his deck (bottom side)

## Good game tests

1) your cards are going in proper order (the deck order)
2) compare the card and have one win
3) draw case

## Organizational matters

Rating criteria:

1. If the software works well
2. Object Oriented design (Single Responsibility Principle, use of types)
3. If there are exceptional cases: good handling of exceptions
4. Time of submission (time of delivery)

Submissions:

1. Submissions should be delivered to me, either via email or (MUCH preferred) via GitHub.
2. Time of email is time of submission
3. On GitHub, if I'm having notifications from repo, each push is one submission (much better for both sides)
4. Multiple submissions are allowed, newest one matters as "time of delivery"
