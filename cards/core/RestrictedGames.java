package cards.core;


/**
 * Write a description of class RestrictedGames here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class RestrictedGames
{
   class Game{}
   class Player{}
    
   public Game[] restrictGamesBasedOn(Player[] players) {
       if(players == null) return new Game[]{}; 
       
       switch(players.length) {
           case 0: 
           case 1: return new Game[]{};
           case 2: return {""};
        
        }
       return new Game[]{};
   }
    
}
