package converters;


/**
 * Enumeration class DistanceUnit - four classic SI distance units, four Imperial units as well
 * 
 * @author Tomasz Borek
 * @version May 2017
 */
public enum DistanceUnit
{
    MILLIMETER,
    CENTIMETER,
    METER,
    KILOMETER
}
