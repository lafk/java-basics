package converters;

/**
 * Converts between mm, cm, m and km (metric system distance units)
 * 
 * @author TJB
 * @version May 2017
 */
public class MetricSystemDistanceUnitConverter
{
    /**
     * 10 mm == 1 cm.
     * 
     * @param  mm   millimeters
     * @return     how many centimeters that is
     */
    public int mm2cm(int mm)
    {
        return mm/10;
    }
}